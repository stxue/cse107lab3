import random

trials = 100_000
n = 8
p = 0.6
q = 0.7
occurences = dict()
occurences_probability = dict()
occurences_given_x = dict()
occurences_given_y = dict()

def main():
  run_problem()

  print_joint()
  print("")
  print_conditional_y()
  print("")
  print_conditional_x()

def run_problem():
  # occurences = dict()
  for i in range(trials):
    (x, y) = trial()
    occurences.setdefault((x, y), 0)
    occurences[(x, y)] += 1
  
  generate_probabilities()
  generate_conditionals()
  return

def generate_probabilities():
  for i in range(n + 1):
    for j in range(i + 1):
      # print(str(i) +  ", " + str(j))
      occurences_probability[(i, j)] = occurences.setdefault((i, j), 0) / trials

def generate_conditionals():

  for i in range(n + 1):
    for j in range(i + 1):
      marg_x = marginal_x(i)
      marg_y = marginal_y(j)
      joint = occurences_probability.setdefault((i, j), 0)
      occurences_given_x[(i, j)] = joint / marg_x
      occurences_given_y[(i, j)] = joint / marg_y
      

def marginal_x(x):
  sum = 0
  for i in range(x + 1):
    # print(str(x))
    sum += occurences_probability.setdefault((x, i), 0)

  return sum

def marginal_y(y):
  sum = 0
  for i in range(y, n + 1):
    sum += occurences_probability.setdefault((i, y), 0)
  
  return sum
def print_conditional_x():
  print("Conditional PMF of Y given X")
  print_prob(occurences_given_x)

def print_conditional_y():
  print("Conditional PMF of X given Y")
  print_prob(occurences_given_y)

def print_joint():
  print("Joint PMF of X and Y")
  print_prob(occurences_probability)

def print_prob(d):
  print("   y: ", end = "")
  for i in range(n + 1):
    print(str(i) + "     ", end = "")
  print("\n", end = "")
  print("x " + "-" * ((n+1)*7))
  for i in range(n + 1):
    print(str(i) + " | ", end = "")
    for j in range(i + 1):
      if (j != 0):
        print(" ", end = "")
      percentage = d.setdefault((i, j), 0)
      print("{percentage:.4f}".format(percentage=round(percentage, 4)).lstrip('0'), end = "")
    print("\n", end="")

def trial():
  x = 0
  y = 0

  for i in range(n):
    play_this_week = play_week()
    if (play_this_week):
      win_this_week = win_week()
      x += 1
      if (win_this_week):
        y += 1

  return (x, y)

def play_week():
  rand = random.random()
  if (rand > p):
    return False
  else:
    return True

def win_week():
  rand = random.random()
  if (rand > q):
    return False
  else:
    return True

if (__name__ == "__main__"):
  main()
